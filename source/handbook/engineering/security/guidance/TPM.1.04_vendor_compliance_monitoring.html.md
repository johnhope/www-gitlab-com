---
layout: markdown_page
title: "TPM.1.04 - Vendor Compliance Monitoring Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# TPM.1.04 - Vendor Compliance Monitoring

## Control Statement

Maintain a program to monitor service providers’ compliance status at least annually.

## Context

We need to validate a third party's compliance status on a yearly basis to ensure they are also complying with compliance requirements. This will assist in obtaining new customers and help maintain assurance with our current customers.

## Scope

All third party service providers that fall within the GitLab Control Framework (GCF).

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TPM.1.04_vendor_compliance_monitoring.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TPM.1.04_vendor_compliance_monitoring.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TPM.1.04_vendor_compliance_monitoring.md).

## Framework Mapping

* PCI
  * 12.8.4
